##################################################################
#
# MongoDB Atlas
#
##################################################################
resource "mongodbatlas_project" "mongodb_project" {
  name   = var.atlas_project_name
  org_id = var.atlas_organization_id
}

resource "mongodbatlas_cluster" "mongodb_cluster" {
  name         = var.atlas_cluster_name
  project_id   = mongodbatlas_project.mongodb_project.id
  cluster_type = "REPLICASET"
  replication_specs {
    num_shards = 1
    regions_config {
      region_name     = var.atlas_region
      electable_nodes = 3
      priority        = 7
      read_only_nodes = 0
    }
  }
  cloud_backup                 = true
  auto_scaling_disk_gb_enabled = true
  mongo_db_major_version       = "4.2"

  //Provider Settings "block"
  provider_name               = "AZURE"
  provider_disk_type_name     = var.atlas_cluster_settings["disk_type_name"]
  provider_instance_size_name = var.atlas_cluster_settings["instance_size_name"]
}

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

# DATABASE USER
resource "mongodbatlas_database_user" "mongodb_user" {
  username           = var.admin_username
  password           = random_password.password.result
  project_id         = mongodbatlas_project.mongodb_project.id
  auth_database_name = "admin"

  roles {
    role_name     = "readWriteAnyDatabase"
    database_name = "admin"
  }
  labels {
    key   = "Name"
    value = var.admin_username
  }
  scopes {
    name = mongodbatlas_cluster.mongodb_cluster.name
    type = "CLUSTER"
  }
}


##################################################################
#
# Private endpoint
#
##################################################################
resource "mongodbatlas_privatelink_endpoint" "mongo_pve" {
  project_id    = mongodbatlas_project.mongodb_project.id
  provider_name = var.cloud_name
  region        = var.atlas_region
}

resource "azurerm_private_endpoint" "azure_pve" {
  location            = var.location
  name                = var.azure_pve_name
  resource_group_name = var.resource_group_name
  subnet_id           = var.subnet_id

  private_service_connection {
    is_manual_connection           = true
    name                           = mongodbatlas_privatelink_endpoint.mongo_pve.private_link_service_name
    private_connection_resource_id = mongodbatlas_privatelink_endpoint.mongo_pve.private_link_service_resource_id
    request_message                = "Azure Private Link"
  }
}

resource "mongodbatlas_privatelink_endpoint_service" "mongo_pve_service" {
  project_id                  = mongodbatlas_project.mongodb_project.id
  private_link_id             = mongodbatlas_privatelink_endpoint.mongo_pve.private_link_id
  endpoint_service_id         = azurerm_private_endpoint.azure_pve.id
  private_endpoint_ip_address = azurerm_private_endpoint.azure_pve.private_service_connection.0.private_ip_address
  provider_name               = var.cloud_name
}
