provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
}

provider "mongodbatlas" {
}

# Create network
module "azure-base-networks" {
  source = "git::https://gitlab.com/keltiotechnology/terraform/modules/azure/azure-base-networks?ref=v1.0"
  tags   = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Virtual Network Variables ##
  vnet_name    = var.vnet_name
  network_cidr = var.network_cidr

  ## Public Subnet Variables ##
  public_subnets = var.public_subnets

  ## Private Subnet Variables ##
  private_subnets = var.private_subnets

  ## NAT Gateway Variables ##
  public_subnet_gateway_ip = var.public_subnet_gateway_ip
  public_subnet_gateway    = var.public_subnet_gateway
}

module "azure_mongodb_atlas" {
  source = "../../"

  resource_group_name   = var.resource_group_name
  location              = var.location
  subnet_id             = module.azure-base-networks.private_subnets["private_subnet_db"].id
  atlas_organization_id = var.atlas_organization_id
  atlas_cluster_name    = var.atlas_cluster_name
  atlas_region          = var.atlas_region
  azure_pve_name        = var.azure_pve_name
  cloud_name            = var.cloud_name
  admin_username        = var.admin_username
  atlas_project_name    = var.atlas_project_name
}