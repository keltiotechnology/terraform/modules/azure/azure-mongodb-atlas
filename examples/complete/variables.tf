##################################################################
#
# MongoDB Atlas
#
##################################################################
variable "atlas_region" {
  type        = string
  description = "(Optional) Azure region for mongodb atlas cluster. Default: France Central"
  default     = "FRANCE_CENTRAL"
}

variable "atlas_project_name" {
  type        = string
  description = "(Required) MongoDB Atlas project name"
}

variable "atlas_organization_id" {
  type        = string
  description = "(Required) MongoDB Atlas organization id"
}

variable "atlas_cluster_name" {
  type        = string
  description = "(Required) MongoDB Atlas cluster name"
  default     = "mongo-cluster"
}

variable "azure_pve_name" {
  type        = string
  description = "(Required) Azure private endpoint name"
}

variable "cloud_name" {
  type        = string
  description = "(Required) Cloud Provider name"
  default     = "AZURE"
}

variable "admin_username" {
  type        = string
  description = "(Required) MongoDB admin user"
  default     = "mongo_admin"
}

variable "atlas_cluster_settings" {
  type        = map(string)
  description = "MongoDB azure instance size"
  default = {
    disk_type_name     = "P2"
    instance_size_name = "M10"
  }
}