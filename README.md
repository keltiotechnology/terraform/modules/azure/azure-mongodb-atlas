# azure mongodb atlas

## prerequisites

### az cli

Terraform uses Azure CLI, you need to have it installed and be logged.

```bash
az login
```

### mongodb atlas API keys

Need to use mongodb atlas API KEY, with paid account.

```bash
export MONGODB_ATLAS_PUBLIC_KEY="xxxx"
export MONGODB_ATLAS_PRIVATE_KEY="xxxx"
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_mongodbatlas"></a> [mongodbatlas](#requirement\_mongodbatlas) | 1.0.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | 2.81.0 |
| <a name="provider_mongodbatlas"></a> [mongodbatlas](#provider\_mongodbatlas) | 1.0.1 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.1.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_private_endpoint.azure_pve](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/private_endpoint) | resource |
| [mongodbatlas_cluster.mongodb_cluster](https://registry.terraform.io/providers/mongodb/mongodbatlas/1.0.1/docs/resources/cluster) | resource |
| [mongodbatlas_database_user.mongodb_user](https://registry.terraform.io/providers/mongodb/mongodbatlas/1.0.1/docs/resources/database_user) | resource |
| [mongodbatlas_privatelink_endpoint.mongo_pve](https://registry.terraform.io/providers/mongodb/mongodbatlas/1.0.1/docs/resources/privatelink_endpoint) | resource |
| [mongodbatlas_privatelink_endpoint_service.mongo_pve_service](https://registry.terraform.io/providers/mongodb/mongodbatlas/1.0.1/docs/resources/privatelink_endpoint_service) | resource |
| [mongodbatlas_project.mongodb_project](https://registry.terraform.io/providers/mongodb/mongodbatlas/1.0.1/docs/resources/project) | resource |
| [random_password.password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_admin_username"></a> [admin\_username](#input\_admin\_username) | (Required) MongoDB admin user | `string` | `"mongo_admin"` | no |
| <a name="input_atlas_cluster_name"></a> [atlas\_cluster\_name](#input\_atlas\_cluster\_name) | (Required) MongoDB Atlas cluster name | `string` | `"mongo-cluster"` | no |
| <a name="input_atlas_cluster_settings"></a> [atlas\_cluster\_settings](#input\_atlas\_cluster\_settings) | (Required) MongoDB azure instance size | `map(string)` | <pre>{<br>  "disk_type_name": "P2",<br>  "instance_size_name": "M10"<br>}</pre> | no |
| <a name="input_atlas_organization_id"></a> [atlas\_organization\_id](#input\_atlas\_organization\_id) | (Required) MongoDB Atlas organization id | `string` | n/a | yes |
| <a name="input_atlas_project_name"></a> [atlas\_project\_name](#input\_atlas\_project\_name) | (Required) MongoDB Atlas project name | `string` | n/a | yes |
| <a name="input_atlas_region"></a> [atlas\_region](#input\_atlas\_region) | (Optional) Azure region for mongodb atlas cluster. Default: France Central | `string` | `"FRANCE_CENTRAL"` | no |
| <a name="input_azure_pve_name"></a> [azure\_pve\_name](#input\_azure\_pve\_name) | (Required) Azure private endpoint name | `string` | n/a | yes |
| <a name="input_cloud_name"></a> [cloud\_name](#input\_cloud\_name) | (Required) Cloud Provider name | `string` | `"AZURE"` | no |
| <a name="input_location"></a> [location](#input\_location) | (Optional) Azure region. Default: France Central | `string` | `"France Central"` | no |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | (Required) Target Azure resource group containing the vnet | `string` | n/a | yes |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | (Required) Azure subnet ID | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_id"></a> [cluster\_id](#output\_cluster\_id) | The cluster ID |
| <a name="output_connection_strings"></a> [connection\_strings](#output\_connection\_strings) | Set of connection strings that your applications use to connect to this cluster |
| <a name="output_container_id"></a> [container\_id](#output\_container\_id) | The Network Peering Container ID |
| <a name="output_endpoint_service_name"></a> [endpoint\_service\_name](#output\_endpoint\_service\_name) | n/a |
| <a name="output_interface_endpoint_id"></a> [interface\_endpoint\_id](#output\_interface\_endpoint\_id) | n/a |
| <a name="output_interface_endpoints"></a> [interface\_endpoints](#output\_interface\_endpoints) | n/a |
| <a name="output_mongo_db_version"></a> [mongo\_db\_version](#output\_mongo\_db\_version) | Version of MongoDB the cluster runs, in major-version.minor-version format |
| <a name="output_mongo_password"></a> [mongo\_password](#output\_mongo\_password) | Mongo admin password |
| <a name="output_mongo_uri"></a> [mongo\_uri](#output\_mongo\_uri) | Base connection string for the cluster |
| <a name="output_mongo_uri_updated"></a> [mongo\_uri\_updated](#output\_mongo\_uri\_updated) | Lists when the connection string was last updated |
| <a name="output_mongo_uri_with_options"></a> [mongo\_uri\_with\_options](#output\_mongo\_uri\_with\_options) | connection string for connecting to the Atlas cluster. Includes the replicaSet, ssl, and authSource query parameters in the connection string with values appropriate for the cluster |
| <a name="output_mongo_user"></a> [mongo\_user](#output\_mongo\_user) | Mongo admin username |
| <a name="output_paused"></a> [paused](#output\_paused) | Flag that indicates whether the cluster is paused or not |
| <a name="output_private_endpoint"></a> [private\_endpoint](#output\_private\_endpoint) | n/a |
| <a name="output_private_link_id"></a> [private\_link\_id](#output\_private\_link\_id) | n/a |
| <a name="output_srv_address"></a> [srv\_address](#output\_srv\_address) | Connection string for connecting to the Atlas cluster. The +srv modifier forces the connection to use TLS/SSL |
| <a name="output_state_name"></a> [state\_name](#output\_state\_name) | Current state of the cluster |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
