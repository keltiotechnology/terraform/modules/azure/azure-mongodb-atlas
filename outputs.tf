output "mongo_user" {
  value       = mongodbatlas_database_user.mongodb_user.username
  description = "Mongo admin username"
}

output "mongo_password" {
  value       = random_password.password
  description = "Mongo admin password"
  sensitive   = true
}

output "cluster_id" {
  value       = mongodbatlas_cluster.mongodb_cluster.cluster_id
  description = "The cluster ID"
}

output "mongo_db_version" {
  value       = mongodbatlas_cluster.mongodb_cluster.mongo_db_version
  description = "Version of MongoDB the cluster runs, in major-version.minor-version format"
}

output "mongo_uri" {
  value       = mongodbatlas_cluster.mongodb_cluster.mongo_uri
  description = "Base connection string for the cluster"
}

output "mongo_uri_updated" {
  value       = mongodbatlas_cluster.mongodb_cluster.mongo_uri_updated
  description = "Lists when the connection string was last updated"
}

output "mongo_uri_with_options" {
  value       = mongodbatlas_cluster.mongodb_cluster.mongo_uri_with_options
  description = "connection string for connecting to the Atlas cluster. Includes the replicaSet, ssl, and authSource query parameters in the connection string with values appropriate for the cluster"
}

output "connection_strings" {
  value       = mongodbatlas_cluster.mongodb_cluster.connection_strings
  description = "Set of connection strings that your applications use to connect to this cluster"
}

output "container_id" {
  value       = mongodbatlas_cluster.mongodb_cluster.container_id
  description = "The Network Peering Container ID"
}

output "paused" {
  value       = mongodbatlas_cluster.mongodb_cluster.paused
  description = "Flag that indicates whether the cluster is paused or not"
}

output "srv_address" {
  value       = mongodbatlas_cluster.mongodb_cluster.srv_address
  description = "Connection string for connecting to the Atlas cluster. The +srv modifier forces the connection to use TLS/SSL"
}

output "state_name" {
  value       = mongodbatlas_cluster.mongodb_cluster.state_name
  description = "Current state of the cluster"
}

output "private_link_id" {
  value = mongodbatlas_privatelink_endpoint.mongo_pve.private_link_id
}

output "endpoint_service_name" {
  value = mongodbatlas_privatelink_endpoint.mongo_pve.endpoint_service_name
}

output "interface_endpoints" {
  value = mongodbatlas_privatelink_endpoint.mongo_pve.interface_endpoints
}

output "interface_endpoint_id" {
  value = mongodbatlas_privatelink_endpoint_service.mongo_pve_service.interface_endpoint_id
}

output "private_endpoint" {
  value = {
    connection_name : mongodbatlas_privatelink_endpoint_service.mongo_pve_service.private_endpoint_connection_name,
    ip_address : mongodbatlas_privatelink_endpoint_service.mongo_pve_service.private_endpoint_ip_address,
    resource_id : mongodbatlas_privatelink_endpoint_service.mongo_pve_service.private_endpoint_resource_id
  }
}
